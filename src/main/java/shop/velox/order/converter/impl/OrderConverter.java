package shop.velox.order.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.commons.converter.Converter;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.model.OrderEntity;

@Component
public class OrderConverter implements Converter<OrderEntity, OrderDto> {

  private static final Logger LOG = LoggerFactory.getLogger(OrderConverter.class);
  private final MapperFacade mapperFacade;

  public OrderConverter() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(OrderEntity.class, OrderDto.class)
        .byDefault()
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public OrderDto convertEntityToDto(OrderEntity orderEntity) {
    OrderDto orderDto = mapperFacade.map(orderEntity, OrderDto.class);
    LOG.debug("Converted {} to {}", orderEntity, orderDto);
    return orderDto;
  }

  @Override
  public OrderEntity convertDtoToEntity(OrderDto orderDto) {
    OrderEntity orderEntity = mapperFacade.map(orderDto, OrderEntity.class);
    LOG.debug("Converted {} to {}", orderDto, orderEntity);
    return orderEntity;
  }
}
