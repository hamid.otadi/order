package shop.velox.order.dao;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.model.OrderEntity;

public interface OrderRepository extends MongoRepository<OrderEntity, String> {

  @Override
  <S extends OrderEntity> S save(S entity);

  @Override
  Optional<OrderEntity> findById(String id);

  @Override
  Page<OrderEntity> findAll(Pageable pageable);

  Page<OrderEntity> findAllByCustomerId(String customerId, Pageable pageable);

  Page<OrderEntity> findAllByOrderStatus(Pageable pageable, OrderStatus filter);

  Page<OrderEntity> findAllByCustomerIdAndOrderStatus(String customerId, Pageable pageable, OrderStatus filter);
}
