package shop.velox.order.service.impl;

import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.dao.OrderRepository;
import shop.velox.order.model.OrderEntity;
import shop.velox.order.service.OrderService;

@Component
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;

  public OrderServiceImpl(@Autowired OrderRepository orderRepository) {
    this.orderRepository = orderRepository;
  }

  @Override
  public OrderEntity createOrder(String customerId,OrderEntity order) {
    if(!StringUtils.equals(customerId, order.getCustomerId())){
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "customerId not matching");
    }
    return orderRepository.save(order);
  }

  @Override
  public Optional<OrderEntity> getOrder(String orderId) {
    return orderRepository.findById(orderId);
  }

  @Override
  public Page<OrderEntity> getAllByCustomerId(String customerId, Pageable pageable, OrderStatus filter) {
    if(filter == null) {
      return orderRepository.findAllByCustomerId(customerId, pageable);
    }
    else {
      return  orderRepository.findAllByCustomerIdAndOrderStatus(customerId, pageable, filter);
    }
  }

  @Override
  public Page<OrderEntity> getAll(Pageable pageable, OrderStatus filter) {
    if(filter == null) {
      return orderRepository.findAll(pageable);
    }
    else {
      return orderRepository.findAllByOrderStatus(pageable, filter);
    }

  }

  @Override
  public OrderEntity updateOrder(String orderId, OrderEntity order, String customerId) {
    if(!StringUtils.equals(orderId, order.getId())){
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "id not matching");
    }

    Optional<OrderEntity> orderEntity = orderRepository.findById(orderId);

    //TODO remove throwing exception with returning ResponseEntity with error message
    if(orderEntity.isEmpty()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "order with id: " + orderId + "does not exist");
    }

    //TODO When payment and shipping are introduced, reimplement payment and shipping logic
    if((orderEntity.get().getPaymentStatus() != order.getPaymentStatus() || (orderEntity.get().getShipmentStatus() != order.getShipmentStatus()))) {
        throw new ResponseStatusException(HttpStatus.CONFLICT, "changing payment/shipment status is not allowed");
    }

    return orderRepository.save(order);
  }
}
