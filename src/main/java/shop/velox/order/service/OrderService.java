package shop.velox.order.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.model.OrderEntity;

public interface OrderService {

  @PreAuthorize(
      "(isAnonymous() && #customerId == null)"
          + "|| @veloxAuthorizationEvaluator.isCurrentUserId(authentication, #customerId)"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.order.service.OrderServiceConstants.Authorities).ORDER_ADMIN)"
  )
  OrderEntity createOrder(final String customerId, OrderEntity order);



  @PostAuthorize(
      "(returnObject.isPresent() &&"
          + "  (@veloxAuthorizationEvaluator.isCurrentUserId(authentication, returnObject.get().getCustomerId())))"
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.order.service.OrderServiceConstants.Authorities).ORDER_ADMIN)"
  )
  Optional<OrderEntity> getOrder(String orderId);



  @PreAuthorize(
      "@veloxAuthorizationEvaluator.isCurrentUserId(authentication, #customerId)"
          + " || @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.order.service.OrderServiceConstants.Authorities).ORDER_ADMIN)"
  )
  Page<OrderEntity> getAllByCustomerId(String customerId, Pageable pageable, OrderStatus filter);



  @PreAuthorize(
      "@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.order.service.OrderServiceConstants.Authorities).ORDER_ADMIN)"
  )
  Page<OrderEntity> getAll(Pageable pageable, OrderStatus filter);



  @PreAuthorize(
      "@orderRepository.findById(#orderId).get().getCustomerId().equals(#customerId)"
          + " || @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
          + "       T(shop.velox.order.service.OrderServiceConstants.Authorities).ORDER_ADMIN)"

  )
  OrderEntity updateOrder(String orderId, OrderEntity order, String customerId);

}
