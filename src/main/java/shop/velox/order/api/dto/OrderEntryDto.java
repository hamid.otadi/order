package shop.velox.order.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.availability.api.dto.AvailabilityDto;

public class OrderEntryDto {

  @Schema(description = "Id of the Article")
  @JsonInclude(Include.ALWAYS)
  private String articleId;

  @Schema(description = "Name of the Article")
  @JsonInclude(Include.ALWAYS)
  private String articleName;

  @Schema(description = "Availability")
  @JsonInclude(Include.ALWAYS)
  private AvailabilityDto availability;

  @Schema(description = "ordered Quantity")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal quantity;

  @Schema(description = "Unit Price")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal unitPrice;

  @Schema(description = "Price")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal price;

  public String getArticleId() {
    return articleId;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public String getArticleName() {
    return articleName;
  }

  public void setArticleName(String articleName) {
    this.articleName = articleName;
  }

  public AvailabilityDto getAvailability() { return availability; }

  public void setAvailability(AvailabilityDto availability) { this.availability = availability; }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
