package shop.velox.order.api.dto;

public enum ShipmentStatus {
  NOT_SHIPPED,
  SHIPPED,
  DELIVERED
}
